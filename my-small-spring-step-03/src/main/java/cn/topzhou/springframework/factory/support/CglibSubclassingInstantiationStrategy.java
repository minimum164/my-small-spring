package cn.topzhou.springframework.factory.support;

import cn.topzhou.springframework.factory.config.BeanDefinition;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.NoOp;

import java.lang.reflect.Constructor;

public class CglibSubclassingInstantiationStrategy implements InstantiationStrategy {
    public Object instantiate(BeanDefinition beanDefinition, String beanName, Constructor constructor, Object[] agrs) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(beanDefinition.getBeanClass());
        enhancer.setCallback(new NoOp() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }
        });
        return enhancer.create(constructor.getParameterTypes(), agrs);
    }
}
