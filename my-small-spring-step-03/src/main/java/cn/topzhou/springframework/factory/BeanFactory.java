package cn.topzhou.springframework.factory;

public interface BeanFactory {
    public Object getBean(String beanName);

    public Object getBean(String beanName, Object[] args);
}
