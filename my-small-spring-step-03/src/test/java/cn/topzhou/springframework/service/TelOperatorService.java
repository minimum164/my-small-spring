package cn.topzhou.springframework.service;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TelOperatorService {
    private String company;

    public TelOperatorService(String company) {
        this.company = company;
    }

    public TelOperatorService() {
    }

    public void queryTelOperatorInfo() {
        System.out.println("得到宽带公司名为:"+company);
    }
}
