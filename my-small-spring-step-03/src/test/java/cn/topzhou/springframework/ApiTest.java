package cn.topzhou.springframework;

import cn.topzhou.springframework.factory.config.BeanDefinition;
import cn.topzhou.springframework.factory.support.DefaultListableBeanFactory;
import cn.topzhou.springframework.service.TelOperatorService;
import org.junit.Test;

public class ApiTest {

    @Test
    public void testBeanFactory() {
        //1. 初始化bean factory
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        //2. 注册bean
        BeanDefinition beanDefinition = new BeanDefinition(TelOperatorService.class);
        beanFactory.registerBeanDefinition("telOperatorService", beanDefinition);
        //3. 获取并使用bean
        TelOperatorService service1 = (TelOperatorService) beanFactory.getBean("telOperatorService","联通");
        service1.queryTelOperatorInfo();

    }
}