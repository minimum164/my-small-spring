package cn.topzhou.springframework;

import cn.topzhou.springframework.entity.BeanReference;
import cn.topzhou.springframework.entity.PropertyValue;
import cn.topzhou.springframework.entity.PropertyValues;
import cn.topzhou.springframework.factory.config.BeanDefinition;
import cn.topzhou.springframework.factory.support.DefaultListableBeanFactory;
import cn.topzhou.springframework.service.TelOperatorDao;
import cn.topzhou.springframework.service.TelOperatorService;
import javafx.beans.property.Property;
import org.junit.Test;

public class ApiTest {

    @Test
    public void testBeanFactory() {
        //1. 初始化bean factory
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        //2. 注册bean
        BeanDefinition daoBeanDefinition = new BeanDefinition(TelOperatorDao.class);
        beanFactory.registerBeanDefinition("telOperatorDao", daoBeanDefinition);

        PropertyValues propertyValues = new PropertyValues();
        propertyValues.addPropertyValue(new PropertyValue("location", "广州"));
        propertyValues.addPropertyValue(new PropertyValue("companyCode", "10010"));
        propertyValues.addPropertyValue(new PropertyValue("telOperatorDao", new BeanReference("telOperatorDao")));
        BeanDefinition beanDefinition = new BeanDefinition(TelOperatorService.class, propertyValues);
        beanFactory.registerBeanDefinition("telOperatorService", beanDefinition);
        //3. 获取并使用bean
        TelOperatorService service1 = (TelOperatorService) beanFactory.getBean("telOperatorService");
        service1.queryTelOperatorInfo();

    }
}