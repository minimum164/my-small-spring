package cn.topzhou.springframework.service;

import java.util.HashMap;
import java.util.Map;

public class TelOperatorDao {
    private static Map<String, String> hashMap = new HashMap<>();

    static {
        hashMap.put("10086", "中国移动");
        hashMap.put("10010", "中国联通");
        hashMap.put("10000", "中国电信");
    }

    public String queryTelCompany(String code){
        return hashMap.get(code);
    }
}
