package cn.topzhou.springframework.factory.support;

import cn.topzhou.springframework.factory.BeanFactory;
import cn.topzhou.springframework.factory.config.BeanDefinition;

/**
 * 实现getBean
 * 有一些单例类型的判断 不重复创建对象实例
 */
public abstract class AbstractBeanFactory extends DefaultSingletonBeanRegistry implements BeanFactory {

    public Object getBean(String beanName) {
        return doGetBean(beanName, null);
    }
    public Object getBean(String beanName,Object... args) {
        return doGetBean(beanName, args);
    }

    private Object doGetBean(String beanName, Object[] args) {
        Object bean = getSingletonBean(beanName);
        if (bean != null)
            return bean;
        BeanDefinition beanDefinition = getBeanDefinition(beanName);
        return createBean(beanName, beanDefinition, args);
    }

    protected abstract BeanDefinition getBeanDefinition(String beanName);

    protected abstract Object createBean(String beanName, BeanDefinition beanDefinition,Object[] args);
}
