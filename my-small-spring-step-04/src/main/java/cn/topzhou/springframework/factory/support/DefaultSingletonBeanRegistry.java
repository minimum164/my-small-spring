package cn.topzhou.springframework.factory.support;

import cn.topzhou.springframework.factory.config.SingletonBeanRegistry;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DefaultSingletonBeanRegistry implements SingletonBeanRegistry {
    private final Map<String, Object> singletonObjectMap = new ConcurrentHashMap<String, Object>();

    public Object getSingletonBean(String beanName) {
        return singletonObjectMap.get(beanName);
    }

    protected void addSingletonBean(String beanName, Object singletonObject) {
        singletonObjectMap.put(beanName, singletonObject);
    }
}
