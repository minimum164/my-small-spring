package cn.topzhou.springframework.factory.support;

import cn.topzhou.springframework.exception.BeansException;
import cn.topzhou.springframework.factory.config.BeanDefinition;

import java.lang.reflect.Constructor;

public class SimpleInstantiationStrategy implements InstantiationStrategy {
    public Object instantiate(BeanDefinition beanDefinition, String beanName, Constructor constructor, Object[] agrs) {
        Class beanClass = beanDefinition.getBeanClass();
        try {
            if (null != constructor) {
                return beanClass.getDeclaredConstructor(constructor.getParameterTypes()).newInstance(agrs);
            } else {
               return  beanClass.getDeclaredConstructor().newInstance();
            }
        } catch (Exception e) {
            throw new BeansException("Failed to instantiate [" + beanClass.getName() + "]", e);
        }
    }
}
