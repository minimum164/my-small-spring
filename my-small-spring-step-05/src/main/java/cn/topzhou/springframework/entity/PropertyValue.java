package cn.topzhou.springframework.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PropertyValue {
    private String name;
    private Object value;
}
