package cn.topzhou.springframework.factory.support;

import cn.topzhou.springframework.exception.BeansException;
import cn.topzhou.springframework.factory.config.BeanDefinition;
import cn.topzhou.springframework.io.Resource;
import cn.topzhou.springframework.io.ResourceLoader;

public interface BeanDefinitionReader {
    BeanDefinitionRegistry getRegistry();
    ResourceLoader getResourceLoader();
    void loadBeanDefinitions(Resource resource) throws BeansException;

    void loadBeanDefinitions(Resource... resources) throws BeansException;

    void loadBeanDefinitions(String location) throws BeansException;
}
