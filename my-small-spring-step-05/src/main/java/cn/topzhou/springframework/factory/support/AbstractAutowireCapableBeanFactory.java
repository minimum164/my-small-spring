package cn.topzhou.springframework.factory.support;

import cn.hutool.core.bean.BeanUtil;
import cn.topzhou.springframework.entity.BeanReference;
import cn.topzhou.springframework.entity.PropertyValue;
import cn.topzhou.springframework.entity.PropertyValues;
import cn.topzhou.springframework.exception.BeansException;
import cn.topzhou.springframework.factory.config.BeanDefinition;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Constructor;

/*
 * 用于实现 AbstractBeanFactory 的createBean 实现方法
 * 后期create bean 部分 会越来越复杂 所以要单独抽到一个抽象类里面实现
 * */
@Getter
@Setter
public abstract class AbstractAutowireCapableBeanFactory extends AbstractBeanFactory {
    private InstantiationStrategy instantiationStrategy = new CglibSubclassingInstantiationStrategy();

    public Object createBean(String beanName, BeanDefinition beanDefinition, Object[] args) {
        Object bean;
        try {
            bean = createBeanInstance(beanName, beanDefinition, args);
            //    填充 属性
            applyPropertyValues(beanName, bean, beanDefinition);
        } catch (Exception e) {
            throw new BeansException("Instantiation of bean failed", e);
        }
        addSingletonBean(beanName, bean);
        return bean;
    }

    public Object createBeanInstance(String beanName, BeanDefinition beanDefinition, Object[] args) {
        Constructor constructorToUse = null;
        Class beanClass = beanDefinition.getBeanClass();
        Constructor[] declaredConstructors = beanClass.getDeclaredConstructors();
        for (Constructor ctor : declaredConstructors) {
            if (null != args && ctor.getParameterTypes().length == args.length) {
                constructorToUse = ctor;
                break;
            }
        }
        return getInstantiationStrategy().instantiate(beanDefinition, beanName, constructorToUse, args);
    }

    /**
     * 属性填充
     *
     * @param beanName
     * @param bean
     * @param beanDefinition
     */
    protected void applyPropertyValues(String beanName, Object bean, BeanDefinition beanDefinition) {
        try {

            PropertyValues propertyValues = beanDefinition.getPropertyValues();
            for (PropertyValue property : propertyValues.getPropertyValues()) {
                String propertyName = property.getName();
                Object propertyValue = property.getValue();
                if (propertyValue instanceof BeanReference) {
                    BeanReference referenceValue = (BeanReference) propertyValue;
                    propertyValue = getBean(referenceValue.getBeanName());
                }
                BeanUtil.setFieldValue(bean, propertyName, propertyValue);
            }
        } catch (Exception e) {
            throw new BeansException("Error setting property values:" + beanName);
        }
    }
}
