package cn.topzhou.springframework.factory;

//单例注册表
public interface SingletonBeanRegistry {
    Object getSingleton(String beanName);
}
