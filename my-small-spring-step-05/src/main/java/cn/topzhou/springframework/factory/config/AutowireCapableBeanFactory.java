package cn.topzhou.springframework.factory.config;

import cn.topzhou.springframework.factory.BeanFactory;

public interface AutowireCapableBeanFactory extends BeanFactory {
}
