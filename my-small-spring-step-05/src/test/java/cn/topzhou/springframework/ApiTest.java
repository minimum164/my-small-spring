package cn.topzhou.springframework;

import cn.topzhou.springframework.entity.BeanReference;
import cn.topzhou.springframework.entity.PropertyValue;
import cn.topzhou.springframework.entity.PropertyValues;
import cn.topzhou.springframework.factory.config.BeanDefinition;
import cn.topzhou.springframework.factory.support.DefaultListableBeanFactory;
import cn.topzhou.springframework.factory.support.XmlBeanDefinitionReader;
import cn.topzhou.springframework.service.TelOperatorDao;
import cn.topzhou.springframework.service.TelOperatorService;
import javafx.beans.property.Property;
import org.junit.Test;

public class ApiTest {

    @Test
    public void testBeanFactory() {
        //1. 初始化bean factory
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        //2. 读取配置文件到beanDefinition
        XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(beanFactory);
        xmlBeanDefinitionReader.loadBeanDefinitions("classpath:spring.xml");

        //3. 获取并使用bean
        TelOperatorService service1 =  beanFactory.getBean("telOperatorService",TelOperatorService.class);
        service1.queryTelOperatorInfo();
    }
}