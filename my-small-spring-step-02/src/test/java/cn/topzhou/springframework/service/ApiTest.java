package cn.topzhou.springframework.service;

import cn.topzhou.springframework.factory.config.BeanDefinition;
import cn.topzhou.springframework.factory.support.DefaultListableBeanFactory;
import org.junit.Test;

public class ApiTest {

    @Test
    public void testBeanFactory() {
        //1. 初始化bean factory
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        //2. 注册bean
        BeanDefinition beanDefinition = new BeanDefinition(TelOperatorService.class);
        beanFactory.registerBeanDefinition("telOperatorService", beanDefinition);
        //3. 获取并使用bean
        TelOperatorService service1 = (TelOperatorService) beanFactory.getBean("telOperatorService");
        service1.queryTelOperatorInfo();
        TelOperatorService singletonBean =(TelOperatorService)  beanFactory.getSingletonBean("telOperatorService");
        singletonBean.queryTelOperatorInfo();
        System.out.println(service1.equals(singletonBean));
    }
}