package cn.topzhou.springframework.factory.support;

import cn.topzhou.springframework.factory.BeanFactory;
import cn.topzhou.springframework.factory.config.BeanDefinition;

/**
 * 实现getBean
 * 有一些单例类型的判断 不重复创建对象实例
 */
public abstract class AbstractBeanFactory extends DefaultSingletonBeanRegistry implements BeanFactory {

    public Object getBean(String beanName) {
        Object bean = getSingletonBean(beanName);
        if (bean != null)
            return bean;
        BeanDefinition beanDefinition = getBeanDefinition(beanName);
        return createBean(beanName, beanDefinition);
    }

    protected abstract BeanDefinition getBeanDefinition(String beanName);

    protected abstract Object createBean(String beanName, BeanDefinition beanDefinition);
}
