package cn.topzhou.springframework.factory;

public interface BeanFactory {
    Object getBean(String beanName);
}
