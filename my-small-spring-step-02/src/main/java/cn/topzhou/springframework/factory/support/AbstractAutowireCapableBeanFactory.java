package cn.topzhou.springframework.factory.support;

import cn.topzhou.springframework.exception.BeansException;
import cn.topzhou.springframework.factory.config.BeanDefinition;

/*
 * 用于实现 AbstractBeanFactory 的createBean 实现方法
 * 后期create bean 部分 会越来越复杂 所以要单独抽到一个抽象类里面实现
 * */
public abstract class AbstractAutowireCapableBeanFactory extends AbstractBeanFactory {

    public Object createBean(String beanName, BeanDefinition beanDefinition) {
        Object bean;
        try {
            bean = beanDefinition.getBeanClass().newInstance();
        } catch (Exception e) {
            throw new BeansException("Instantiation of bean failed", e);
        }
        addSingletonBean(beanName, bean);
        return bean;
    }
}
