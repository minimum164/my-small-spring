package cn.topzhou.springframework.entity;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class PropertyValues {
    private List<PropertyValue> propertyValues= new ArrayList<>();

    public void addPropertyValue(PropertyValue pv) {
        this.propertyValues.add(pv);
    }

    public PropertyValue getPropertyValue(final String propertyName) {
        return propertyValues.stream().filter(it -> it.getName().equalsIgnoreCase(propertyName)).findFirst().orElse(null);
    }
}
