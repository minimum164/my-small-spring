package cn.topzhou.springframework.context.support;

import cn.topzhou.springframework.context.ConfigurableApplicationContext;
import cn.topzhou.springframework.exception.BeansException;
import cn.topzhou.springframework.factory.ConfigurableListableBeanFactory;
import cn.topzhou.springframework.factory.config.BeanFactoryPostProcessor;
import cn.topzhou.springframework.factory.config.BeanPostProcessor;
import cn.topzhou.springframework.io.DefaultResourceLoader;
import org.omg.SendingContext.RunTime;

import java.util.Map;

public abstract class AbstractApplicationContext extends DefaultResourceLoader implements ConfigurableApplicationContext {
    @Override
    public void refresh() {
        //    1. 创建BeanFactory，加载beanDefinition
        refreshBeanFactory();
        //    2. 获取BeanFactory
        ConfigurableListableBeanFactory beanFactory = getBeanFactory();
        //    3. 执行 BeanFactoryPostProcesses
        invokeBeanFactoryPostProcessors(beanFactory);
        //    4.注册 BeanPostProcesse 到BeanFactory
        registerBeanPostProcessors(beanFactory);
        //    5. 实例化bean
        beanFactory.preInstantiateSingletons();
    }

    @Override
    public void registerShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(this::close));
    }

    @Override
    public void close() {
        getBeanFactory().destroySingletons();
    }

    //留给管理BeanFactory 的子类处理 实现细节
    protected abstract void refreshBeanFactory() throws BeansException;

    //实现类中会单独管理BeanFactory
    protected abstract ConfigurableListableBeanFactory getBeanFactory();

    private void invokeBeanFactoryPostProcessors(ConfigurableListableBeanFactory beanFactory) {
        Map<String, BeanFactoryPostProcessor> beanFactoryPostProcessorMap = beanFactory.getBeansOfType(BeanFactoryPostProcessor.class);
        for (BeanFactoryPostProcessor beanFactoryPostProcessor : beanFactoryPostProcessorMap.values()) {
            beanFactoryPostProcessor.postProcessBeanFactory(beanFactory);
        }
    }

    //在实例化前后处理 BeanPostProcessor， 所以要将它交给 beanFactory 主流程处理
    private void registerBeanPostProcessors(ConfigurableListableBeanFactory beanFactory) {
        Map<String, BeanPostProcessor> beanPostProcessorMap = beanFactory.getBeansOfType(BeanPostProcessor.class);
        for (BeanPostProcessor beanPostProcessor : beanPostProcessorMap.values()) {
            beanFactory.addBeanPostProcessor(beanPostProcessor);
        }
    }

    @Override
    public <T> Map<String, T> getBeansOfType(Class<T> type) throws BeansException {
        return getBeanFactory().getBeansOfType(type);
    }

    @Override
    public String[] getBeanDefinitionNames() {
        return getBeanFactory().getBeanDefinitionNames();
    }

    @Override
    public Object getBean(String beanName) {
        return getBeanFactory().getBean(beanName);
    }

    @Override
    public Object getBean(String name, Object... args) throws BeansException {
        return getBeanFactory().getBean(name, args);
    }

    @Override
    public <T> T getBean(String name, Class<T> requiredType) throws BeansException {
        return getBeanFactory().getBean(name, requiredType);
    }
}
