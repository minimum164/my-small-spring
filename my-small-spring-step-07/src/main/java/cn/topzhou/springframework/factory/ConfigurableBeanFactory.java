package cn.topzhou.springframework.factory;

import cn.topzhou.springframework.factory.config.BeanPostProcessor;
import cn.topzhou.springframework.factory.config.HierarchicalBeanFactory;
import cn.topzhou.springframework.factory.config.SingletonBeanRegistry;

public interface ConfigurableBeanFactory extends HierarchicalBeanFactory, SingletonBeanRegistry {
    String SCOPE_SINGLETON = "singleton";

    String SCOPE_PROTOTYPE = "prototype";

    void addBeanPostProcessor(BeanPostProcessor beanPostProcessor);
}
