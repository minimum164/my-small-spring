package cn.topzhou.springframework.factory.support;

import cn.topzhou.springframework.exception.BeansException;
import cn.topzhou.springframework.factory.DisposableBean;
import cn.topzhou.springframework.factory.config.SingletonBeanRegistry;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class DefaultSingletonBeanRegistry implements SingletonBeanRegistry {
    private final Map<String, Object> singletonObjectMap = new ConcurrentHashMap<String, Object>();
    private final Map<String, DisposableBean> disposableBeanMap = new ConcurrentHashMap<String, DisposableBean>();

    public Object getSingletonBean(String beanName) {
        return singletonObjectMap.get(beanName);
    }

    protected void addSingletonBean(String beanName, Object singletonObject) {
        singletonObjectMap.put(beanName, singletonObject);
    }

    public void registerDisposableBean(String beanName, DisposableBean disposableBean) {
        disposableBeanMap.put(beanName, disposableBean);
    }

    /*
     * 统一销毁bean
     * */
    @Override
    public void destroySingletons() {
        Set<String> keySet = this.disposableBeanMap.keySet();
        Object[] beanNames = keySet.toArray();
        int length = beanNames.length;
        for (int i = length - 1; i >= 0; i--) {
            Object beanName = beanNames[i];
            DisposableBean disposableBean = disposableBeanMap.remove(beanName);
            try {
                disposableBean.destroy();
            } catch (Exception e) {
                throw new BeansException("Destroy method on bean with name '" + beanName + "' threw an exception", e);
            }
        }
    }
}
