package cn.topzhou.springframework.factory;

public interface DisposableBean {
    //销毁bean
    void destroy() throws Exception;
}
