package cn.topzhou.springframework.factory.config;

/**
 * Bean 对象实例化之后修改 Bean 对象
 */
public interface BeanPostProcessor {
    Object postProcessBeforeInitialization(Object bean, String beanName);

    Object postProcessAfterInitialization(Object bean, String beanName);
}
