package cn.topzhou.springframework.factory.config;

/**
 * 单例注册表
 */
public interface SingletonBeanRegistry {
    Object getSingletonBean(String beanName);

    void destroySingletons();
}
