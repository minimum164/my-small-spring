package cn.topzhou.springframework.factory.config;

import cn.topzhou.springframework.factory.BeanFactory;

public interface HierarchicalBeanFactory extends BeanFactory {
}
