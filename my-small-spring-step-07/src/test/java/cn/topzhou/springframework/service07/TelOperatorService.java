package cn.topzhou.springframework.service07;

import cn.topzhou.springframework.factory.DisposableBean;
import cn.topzhou.springframework.factory.InitializingBean;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TelOperatorService implements InitializingBean, DisposableBean {
    private String location;
    private String companyCode;
    private String area;
    private cn.topzhou.springframework.service07.TelOperatorDao telOperatorDao;


    public TelOperatorService() {
        System.out.println("执行 TelOperatorService 初始化方法。");
    }

    public void queryTelOperatorInfo() {
        System.out.println("查询该宽带公司 ：" + telOperatorDao.queryTelCompany(companyCode) + " 位于 " + location + area);
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("执行TelOperatorService 销毁方法。");

    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }
}
