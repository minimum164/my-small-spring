package cn.topzhou.springframework.common;

import cn.topzhou.springframework.entity.PropertyValue;
import cn.topzhou.springframework.entity.PropertyValues;
import cn.topzhou.springframework.exception.BeansException;
import cn.topzhou.springframework.factory.ConfigurableListableBeanFactory;
import cn.topzhou.springframework.factory.config.BeanDefinition;
import cn.topzhou.springframework.factory.config.BeanFactoryPostProcessor;

public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        BeanDefinition beanDefinition = beanFactory.getBeanDefinition("telOperatorService");
        PropertyValues propertyValues = beanDefinition.getPropertyValues();
        propertyValues.addPropertyValue(new PropertyValue("area", "改为：天河"));
    }
}
