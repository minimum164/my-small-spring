package cn.topzhou.springframework;

import cn.topzhou.springframework.context.support.ClassPathXmlApplicationContext;
import cn.topzhou.springframework.service09.TelOperatorService;
import org.junit.Test;

public class ApiTest {

    @Test
    public void testBeanFactory() {
        // 1.初始化 BeanFactory
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
        TelOperatorService  telOperatorService= applicationContext.getBean("telOperatorService",TelOperatorService.class);
        System.out.println(telOperatorService.queryTelOperatorInfo());

    }
}