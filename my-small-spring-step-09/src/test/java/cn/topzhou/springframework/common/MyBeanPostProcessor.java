package cn.topzhou.springframework.common;

import cn.topzhou.springframework.factory.config.BeanPostProcessor;
import cn.topzhou.springframework.service09.TelOperatorService;

public class MyBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        return bean;
    }
}
