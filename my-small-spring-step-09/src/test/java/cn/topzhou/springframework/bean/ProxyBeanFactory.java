package cn.topzhou.springframework.bean;

import cn.topzhou.springframework.factory.FactoryBean;
import cn.topzhou.springframework.service09.ITelOperatorDao;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class ProxyBeanFactory implements FactoryBean<ITelOperatorDao> {
    @Override
    public ITelOperatorDao getObject() throws Exception {
        InvocationHandler handler = (proxy, method, args) -> {

            // 添加排除方法
            if ("toString".equals(method.getName())) return this.toString();

            Map<String, String> hashMap = new HashMap<>();
            hashMap.put("10086", "移动");
            hashMap.put("10000", "电信");
            hashMap.put("10010", "联通");

            return "你被代理了 " + method.getName() + "：" + hashMap.get(args[0].toString());
        };
        return (ITelOperatorDao) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{ITelOperatorDao.class}, handler);
    }
    @Override
    public Class<?> getObjectType() {
        return ITelOperatorDao.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
