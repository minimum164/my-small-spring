package cn.topzhou.springframework.service09;

public interface ITelOperatorDao {

    String queryTelCompany(String code);
}
