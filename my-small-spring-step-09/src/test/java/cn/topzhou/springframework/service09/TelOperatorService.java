package cn.topzhou.springframework.service09;

public class TelOperatorService  {
    private String uId;
    private String location;
    private ITelOperatorDao iTelOperatorDao;
    public String queryTelOperatorInfo() {
        return iTelOperatorDao.queryTelCompany(uId) + ",位于" + location;
    }
}
