package cn.topzhou.springframework.context;

public interface ConfigurableApplicationContext extends ApplicationContext {
    //刷新容器
    void refresh();

    //注册或者注销钩子
    void registerShutdownHook();

    //注销所有对象
    void close();
}
