package cn.topzhou.springframework.context.support;

import cn.topzhou.springframework.context.support.AbstractApplicationContext;
import cn.topzhou.springframework.exception.BeansException;
import cn.topzhou.springframework.factory.ConfigurableListableBeanFactory;
import cn.topzhou.springframework.factory.support.DefaultListableBeanFactory;

/**
 * 用于管理beanFactory 对象的生成 与获取
 */
public abstract class AbstractRefreshableApplicationContext extends AbstractApplicationContext {
    private DefaultListableBeanFactory beanFactory;

    @Override
    protected void refreshBeanFactory() throws BeansException {
        DefaultListableBeanFactory beanFactory = createBeanFactory();
        loadBeanDefinitions(beanFactory);
        this.beanFactory = beanFactory;
    }

    private DefaultListableBeanFactory createBeanFactory() {
        return new DefaultListableBeanFactory();
    }

    protected abstract void loadBeanDefinitions(DefaultListableBeanFactory beanFactory);
    @Override
    protected ConfigurableListableBeanFactory getBeanFactory() {
        return beanFactory;
    }
}
