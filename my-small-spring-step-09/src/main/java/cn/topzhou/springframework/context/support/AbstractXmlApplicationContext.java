package cn.topzhou.springframework.context.support;

import cn.topzhou.springframework.factory.support.DefaultListableBeanFactory;
import cn.topzhou.springframework.factory.support.XmlBeanDefinitionReader;

public abstract class AbstractXmlApplicationContext extends AbstractRefreshableApplicationContext {

    @Override
    protected void loadBeanDefinitions(DefaultListableBeanFactory beanFactory) {
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory, this);
        String[] locations = getConfigLocations();
        if (locations != null) {
            reader.loadBeanDefinitions(locations);
        }
    }

    // 从入口上下文类 , 才能拿到 具体地址信息， 入口实现类用
    protected abstract String[] getConfigLocations();

}
