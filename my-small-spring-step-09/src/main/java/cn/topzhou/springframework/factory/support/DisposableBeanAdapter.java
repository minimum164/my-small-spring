package cn.topzhou.springframework.factory.support;

import cn.hutool.core.util.StrUtil;
import cn.topzhou.springframework.exception.BeansException;
import cn.topzhou.springframework.factory.DisposableBean;
import cn.topzhou.springframework.factory.config.BeanDefinition;

import java.lang.reflect.Method;

public class DisposableBeanAdapter implements DisposableBean {
    private final Object bean;
    private final String beanName;
    private String destroyMethodName;

    public DisposableBeanAdapter(Object bean, String beanName, BeanDefinition beanDefinition) {
        this.bean = bean;
        this.beanName = beanName;
        this.destroyMethodName = beanDefinition.getDestroyMethodName();
    }

    @Override
    public void destroy() throws Exception {
        //    1. 如果是 接口实现类型 处理
        if (bean instanceof DisposableBean) {
            DisposableBean dpBean = (DisposableBean) bean;
            dpBean.destroy();
        }
        //    2. 如果是 配置文件实现 处理
        if (StrUtil.isNotBlank(destroyMethodName)
                //        避免重复销毁，判断不是接口实现类型
                && !(bean instanceof DisposableBean && "destroy".equals(this.destroyMethodName))){
            Method destroyMethod = bean.getClass().getMethod(destroyMethodName);
            if (null == destroyMethod) {
                throw new BeansException("Couldn't find a destroy method named '" + destroyMethodName + "' on bean with name '" + beanName + "'");
            }
            destroyMethod.invoke(bean);
        }
    }
}
