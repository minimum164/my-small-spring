package cn.topzhou.springframework.factory;

public interface FactoryBean<T> {
    T getObject() throws Exception;

    Class<?> getObjectType();

    /**
     * 是否是单例类型
     *
     * @return
     */
    boolean isSingleton();
}
