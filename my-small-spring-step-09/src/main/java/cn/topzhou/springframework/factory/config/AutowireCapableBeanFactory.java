package cn.topzhou.springframework.factory.config;

import cn.topzhou.springframework.exception.BeansException;
import cn.topzhou.springframework.factory.BeanFactory;

public interface AutowireCapableBeanFactory extends BeanFactory {
    //执行前置方法
    Object applyBeanPostProcessorsBeforeInitialization(Object existingBean, String beanName) throws BeansException;

    Object applyBeanPostProcessorsAfterInitialization(Object existingBean, String beanName) throws BeansException;

}
