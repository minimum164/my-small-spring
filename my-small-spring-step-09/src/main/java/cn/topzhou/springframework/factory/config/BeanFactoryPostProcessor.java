package cn.topzhou.springframework.factory.config;

import cn.topzhou.springframework.exception.BeansException;
import cn.topzhou.springframework.factory.ConfigurableListableBeanFactory;

public interface BeanFactoryPostProcessor {
    //在BeanDefinition 加载完成以后， 实例化 bean执行
    void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException;
}
