package cn.topzhou.springframework.factory.support;

import cn.topzhou.springframework.factory.config.BeanDefinition;

import java.lang.reflect.Constructor;

/**
 * 定义一个实例化策略接口
 * 提供给不同实例化方法实现 包括CGLIB 和 反射
 */
public interface InstantiationStrategy {
    Object instantiate(BeanDefinition beanDefinition, String beanName, Constructor constructor, Object[] agrs);
}
