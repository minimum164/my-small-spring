package cn.topzhou.springframework.factory;

import cn.topzhou.springframework.factory.Aware;

public interface BeanNameAware extends Aware {
    void setBeanName(String beanName);
}
