package cn.topzhou.springframework.factory;

import cn.topzhou.springframework.factory.Aware;

public interface BeanClassLoaderAware extends Aware {
    void setBeanClassLoader(ClassLoader classLoader);
}
