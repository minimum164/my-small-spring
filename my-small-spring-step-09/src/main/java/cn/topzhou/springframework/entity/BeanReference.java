package cn.topzhou.springframework.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BeanReference {
    private String beanName;
}
