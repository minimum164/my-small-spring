package cn.topzhou.springframework.service07;

import java.util.HashMap;
import java.util.Map;

public class TelOperatorDao {
    private static Map<String, String> hashMap = new HashMap<>();

    public void destroyDataMethod() {
        System.out.println("执行TelOperatorDao：destroy-method");
        hashMap.clear();
    }

    public void initDataMethod() {
        System.out.println("初始化TelOperatorDao：initD-Method");
        hashMap.put("10086", "中国移动");
        hashMap.put("10010", "中国联通");
        hashMap.put("10000", "中国电信");
    }

    public String queryTelCompany(String code) {
        return hashMap.get(code);
    }
}
