package cn.topzhou.springframework.service07;

import cn.topzhou.springframework.context.ApplicationContext;
import cn.topzhou.springframework.factory.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TelOperatorService implements BeanNameAware, BeanClassLoaderAware, ApplicationContextAware, BeanFactoryAware, InitializingBean, DisposableBean {
    private String location;
    private String companyCode;
    private String area;
    private cn.topzhou.springframework.service07.TelOperatorDao telOperatorDao;
    private ApplicationContext applicationContext;
    private BeanFactory beanFactory;


    public TelOperatorService() {
        System.out.println("执行 TelOperatorService 初始化方法。");
    }

    public void queryTelOperatorInfo() {
        System.out.println("查询该宽带公司 ：" + telOperatorDao.queryTelCompany(companyCode) + " 位于 " + location + area);
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("执行TelOperatorService 销毁方法。");

    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {
        System.out.println("Current classLoader =" + classLoader);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @Override
    public void setBeanName(String beanName) {
        System.out.println("Current bean name =" + beanName);
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }
}
