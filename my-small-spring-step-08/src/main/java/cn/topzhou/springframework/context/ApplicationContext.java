package cn.topzhou.springframework.context;

import cn.topzhou.springframework.factory.config.ListableBeanFactory;
//ApplicationContext 继承自 BeanFactory
public interface ApplicationContext extends ListableBeanFactory {
}
