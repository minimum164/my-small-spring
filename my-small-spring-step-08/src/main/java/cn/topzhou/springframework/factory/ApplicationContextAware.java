package cn.topzhou.springframework.factory;

import cn.topzhou.springframework.context.ApplicationContext;

public interface ApplicationContextAware extends Aware {
    void setApplicationContext(ApplicationContext applicationContext);
}
