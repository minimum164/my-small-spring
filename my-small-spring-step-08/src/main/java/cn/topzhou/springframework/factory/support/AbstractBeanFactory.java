package cn.topzhou.springframework.factory.support;

import cn.topzhou.springframework.exception.BeansException;
import cn.topzhou.springframework.factory.BeanFactory;
import cn.topzhou.springframework.factory.ConfigurableBeanFactory;
import cn.topzhou.springframework.factory.config.BeanDefinition;
import cn.topzhou.springframework.factory.config.BeanPostProcessor;
import cn.topzhou.springframework.util.ClassUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 实现getBean
 * 有一些单例类型的判断 不重复创建对象实例
 * 管理前置后置处理器BeanPostProcessor
 */
public abstract class AbstractBeanFactory extends DefaultSingletonBeanRegistry implements ConfigurableBeanFactory {
    /**
     * BeanPostProcessors to apply in createBean
     */
    private final List<BeanPostProcessor> beanPostProcessors = new ArrayList<BeanPostProcessor>();
    private ClassLoader beanClassLoader = ClassUtils.getDefaultClassLoader();

    @Override
    public Object getBean(String beanName) {
        return doGetBean(beanName, null);
    }

    @Override
    public Object getBean(String beanName, Object... args) {
        return doGetBean(beanName, args);
    }

    @Override
    public <T> T getBean(String name, Class<T> requiredType) {
        return (T) getBean(name);
    }

    private Object doGetBean(String beanName, Object[] args) {
        Object bean = getSingletonBean(beanName);
        if (bean != null)
            return bean;
        BeanDefinition beanDefinition = getBeanDefinition(beanName);
        return createBean(beanName, beanDefinition, args);
    }

    protected abstract BeanDefinition getBeanDefinition(String beanName);

    protected abstract Object createBean(String beanName, BeanDefinition beanDefinition, Object[] args);

    @Override
    public void addBeanPostProcessor(BeanPostProcessor beanPostProcessor) {
        beanPostProcessors.remove(beanPostProcessor);
        beanPostProcessors.add(beanPostProcessor);
    }

    public List<BeanPostProcessor> getBeanPostProcessors() {
        return this.beanPostProcessors;
    }
    public ClassLoader getBeanClassLoader() {
        return this.beanClassLoader;
    }
}
