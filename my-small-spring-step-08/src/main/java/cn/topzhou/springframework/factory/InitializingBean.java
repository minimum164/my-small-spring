package cn.topzhou.springframework.factory;

/**
 * 统一初始化处理接口
 */
public interface InitializingBean {
    void afterPropertiesSet() throws Exception;
}
