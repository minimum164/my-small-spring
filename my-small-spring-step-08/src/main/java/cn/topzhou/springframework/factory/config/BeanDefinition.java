package cn.topzhou.springframework.factory.config;


import cn.topzhou.springframework.entity.PropertyValues;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BeanDefinition {

    private Class beanClass;
    private PropertyValues propertyValues;
    private String initMethodName;
    private String destroyMethodName;

    public BeanDefinition(Class beanClass) {
        this.propertyValues = new PropertyValues();
        this.beanClass = beanClass;
    }

    public BeanDefinition(Class beanClass, PropertyValues propertyValues) {
        this.beanClass = beanClass;
        this.propertyValues = propertyValues != null ? propertyValues : new PropertyValues();
    }
}
