package cn.topzhou.springframework.factory;

import cn.topzhou.springframework.factory.config.BeanPostProcessor;
import cn.topzhou.springframework.factory.config.HierarchicalBeanFactory;

public interface ConfigurableBeanFactory extends HierarchicalBeanFactory, SingletonBeanRegistry {
    String SCOPE_SINGLETON = "singleton";

    String SCOPE_PROTOTYPE = "prototype";

    void addBeanPostProcessor(BeanPostProcessor beanPostProcessor);
}
