package cn.topzhou.springframework.factory;


import cn.topzhou.springframework.exception.BeansException;
import cn.topzhou.springframework.factory.config.AutowireCapableBeanFactory;
import cn.topzhou.springframework.factory.config.BeanDefinition;
import cn.topzhou.springframework.factory.config.ListableBeanFactory;


public interface ConfigurableListableBeanFactory extends ListableBeanFactory, AutowireCapableBeanFactory, ConfigurableBeanFactory {

    BeanDefinition getBeanDefinition(String beanName) throws BeansException;

    void preInstantiateSingletons() throws BeansException;
}
