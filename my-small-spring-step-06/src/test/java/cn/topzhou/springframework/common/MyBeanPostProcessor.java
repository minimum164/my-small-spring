package cn.topzhou.springframework.common;

import cn.topzhou.springframework.factory.config.BeanPostProcessor;
import cn.topzhou.springframework.service.TelOperatorService;

public class MyBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        if (beanName.equalsIgnoreCase("telOperatorService")) {
            TelOperatorService telOperatorService = (TelOperatorService) bean;
            telOperatorService.setCompanyCode("10010");
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        return bean;
    }
}
