package cn.topzhou.springframework;

import cn.topzhou.springframework.context.support.ClassPathXmlApplicationContext;
import cn.topzhou.springframework.entity.BeanReference;
import cn.topzhou.springframework.entity.PropertyValue;
import cn.topzhou.springframework.entity.PropertyValues;
import cn.topzhou.springframework.factory.config.BeanDefinition;
import cn.topzhou.springframework.factory.support.DefaultListableBeanFactory;
import cn.topzhou.springframework.factory.support.XmlBeanDefinitionReader;
import cn.topzhou.springframework.service.TelOperatorDao;
import cn.topzhou.springframework.service.TelOperatorService;
import javafx.beans.property.Property;
import org.junit.Test;

public class ApiTest {

    @Test
    public void testBeanFactory() {
        // 1.初始化 BeanFactory
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
        TelOperatorService  telOperatorService= applicationContext.getBean("telOperatorService",TelOperatorService.class);
        telOperatorService.queryTelOperatorInfo();
    }
}