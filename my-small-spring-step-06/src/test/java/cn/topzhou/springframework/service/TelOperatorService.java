package cn.topzhou.springframework.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TelOperatorService {
    private String location;
    private String companyCode;
    private String area;
    private TelOperatorDao telOperatorDao;


    public TelOperatorService() {
    }

    public void queryTelOperatorInfo() {
        System.out.println("查询该宽带公司 ：" + telOperatorDao.queryTelCompany(companyCode) + " 位于 " + location + area);
    }
}
