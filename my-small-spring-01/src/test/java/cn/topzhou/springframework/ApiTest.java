package cn.topzhou.springframework;

import cn.topzhou.springframework.service.TelOperatorService;
import org.junit.Test;

public class ApiTest {

    @Test
    public void testBeanFactory() {
        //1. 初始化bean factory
        BeanFactory beanFactory = new BeanFactory();
        //2. 注册bean
        BeanDefinition beanDefinition = new BeanDefinition(new TelOperatorService());
        beanFactory.registerBeanDefinition("telOperatorService", beanDefinition);
        //3. 获取并使用bean
        TelOperatorService service = (TelOperatorService) beanFactory.getBean("telOperatorService");
        service.queryTelOperatorInfo();
    }
}